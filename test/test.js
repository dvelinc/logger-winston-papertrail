'use strict';

const Logger = require('../logger');

const logger = new Logger({
  debug: true,
  stackTraceLimit: 5,
});

console.log(logger);

logger.overrideConsole();

setTimeout(() => {
  console.error('sdf');
}, 1000);
