'use strict';

const winston = require('winston');
/**
 * Requiring `winston-papertrail` will expose
 * `winston.transports.Papertrail`
 */
require('winston-papertrail').Papertrail; // eslint-disable-line

const util = require('util');

function Logger(options) {
  const that = this;
  that.debug = !!options.debug;

  if (options.stackTraceLimit) {
    // only show first 4 rows of an error trace to reduce log size
    Error.stackTraceLimit = options.stackTraceLimit;
  }

  // create all transporter
  const transports = [];
  const consoleTransporter = new (winston.transports.Console)({
    level: process.env.LOG_LEVEL_CONSOLE || 'debug',
    handleExceptions: true,
    colorize: true,
    inlineMeta: false,
  });
  transports.push(consoleTransporter);
  if (options.papertrail) {
    const ptTransporter = new winston.transports.Papertrail({
      level: process.env.LOG_LEVEL_PAPERTRAIL || options.papertrail.level || 'debug',
      handleExceptions: true,
      host: options.papertrail.host,
      port: options.papertrail.port,
      program: options.papertrail.program,
      colorize: true,
      inlineMeta: false,
    });
    // papertail event logs
    if (that.debug) {
      ptTransporter.on('error', (err) => that.winstonLogger && that.winstonLogger.error(err));
      ptTransporter.on('connect', (message) => that.winstonLogger && that.winstonLogger.info(message));
    }
    transports.push(ptTransporter);
  }
  that.winstonLogger = new (winston.Logger)({
    exitOnError: false,
    transports,
  });
  // Handle errors
  that.winstonLogger.on('error', (error) => {
    console.error(error);
  });

  // used to override the console.log
  that.overrideConsole = function overrideConsole() {
    /**
     * @param args arguments from a function
     * @param prefix element to add as the first parameter
     * @returns array of parameters
     */
    function formatArgs(args, prefix) {
      const argsArray = [util.format.apply(util.format, Array.prototype.slice.call(args))];
      if (prefix) {
        argsArray.unshift(prefix); // add stage and level at the beginning of the array
      }
      return argsArray;
    }

    console.debug = function debug() {
      that.winstonLogger.log.apply(that.winstonLogger, formatArgs(arguments, 'debug'));
    };
    console.verbose = function debug() {
      that.winstonLogger.debug.apply(that.winstonLogger, formatArgs(arguments, 'verbose'));
    };
    console.log = function log() {
      that.winstonLogger.info.apply(that.winstonLogger, formatArgs(arguments, 'info'));
    };
    console.info = function info() {
      that.winstonLogger.info.apply(that.winstonLogger, formatArgs(arguments, 'info'));
    };
    console.warn = function warn() {
      that.winstonLogger.warn.apply(that.winstonLogger, formatArgs(arguments, 'warn'));
    };
    console.error = function error() {
      that.winstonLogger.error.apply(that.winstonLogger, formatArgs(arguments, 'error'));
    };
  };

  /**
   * overrides the console.* functions to fit our CloudWatch -> Papertrail format
   * Format: [STAGE]\t[LEVEL]\t[MESSAGE, ...]
   * @param stage development, staging, production, ...
   */
  that.overrideConsoleForCloudwatchHarvester = function overrideConsoleForCloudwatchHarvester(stage) {
    if (String(console.log).indexOf('originalConsole.') > -1) {
      // do not override it again
      return;
    }

    const originalConsole = {
      log: console.log,
      info: console.info,
      warn: console.warn,
      error: console.error,
    };

    function formatArgs(args, prefix) {
      const argsArray = [util.format.apply(util.format, Array.prototype.slice.call(args))];
      if (prefix) {
        argsArray.unshift(prefix); // add stage and level at the beginning of the array
      }
      return argsArray;
    }

    console.debug = function debug() {
      originalConsole.log.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[DEBUG]\t`));
    };
    console.verbose = function verbose() {
      originalConsole.log.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[VERBOSE]\t`));
    };
    console.log = function log() {
      originalConsole.log.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[INFO]\t`));
    };
    console.info = function info() {
      originalConsole.info.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[INFO]\t`));
    };
    console.warn = function warn() {
      originalConsole.warn.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[WARN]\t`));
    };
    console.error = function error() {
      originalConsole.error.apply(originalConsole, formatArgs(arguments, `${stage || 'unknown'}\t[ERROR]\t`));
    };
  };
}

/**
 * get other configs from here:
 * https://github.com/timgit/pg-boss/wiki/configuration
 * @param options object {
 *   stackTraceLimit: 4, // error stack trace limit
 *   papertrail: {
 *     level: 'debug', // or put it in: process.env.LOG_LEVEL_PAPERTRAIL
 *     host: '',
 *     port: 1234,
 *     program: 'name',
 *   }
 * }
 * @returns {Logger}
 */
module.exports = function init(options) {
  if (options && options.papertrail) {
    if (!options.papertrail.host || !options.papertrail.port || !options.papertrail.program) {
      throw new Error('Logger.init: options { host, port, program } parameter missing');
    }
  }
  return new Logger(options || {});
};
